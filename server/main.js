import { Meteor } from 'meteor/meteor';
import '../imports/api/questions.js';
import '../imports/api/answers.js';
import '../imports/api/server/publications.js';
//import { render } from 'react-dom';
//import { renderRoutes } from '../imports/startup/client/routes.js';
Meteor.startup(() => {
  // code to run on server at startup
  //render(renderRoutes(), document.getElementById('main'));
});
