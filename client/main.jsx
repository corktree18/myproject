//import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';
import '../imports/startup/accounts-config.js';
 import { renderRoutes } from '../imports/startup/client/routes.js';
//import App from '../imports/ui/App.jsx';
//import SidebarContainer from '../imports/ui/containers/SidebarContainer.js';


Meteor.startup(() => {
  //render(<SidebarContainer />, document.getElementById('sidebar'));
  render(renderRoutes(), document.getElementById('main'));
});