import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'
import AboutContainer from '../../ui/containers/AboutContainer.js';
import AskContainer from '../../ui/containers/AskContainer.js';
import HomeContainer from '../../ui/containers/HomeContainer.js';
import MapContainer from '../../ui/containers/MapContainer.js';
import QuestionsContainer from '../../ui/containers/QuestionsContainer.js';
import AccountsUIWrapper from '../../ui/AccountsUIWrapper.jsx';


export const renderRoutes = () => (
  //<Router>の中身のエレメントは必ず一つ
  	<Router>
  		<div>
		  	<div className="sidebar col-sm-2">
		  		<AccountsUIWrapper />
				<div className="title"><h2>Ask To All</h2></div>
				<div className="menu">
					<Link to="/about">About</Link>
					<Link to="/ask">Ask</Link>
					<Link to="/questions">Questions</Link>
					<Link to="/settings">Settings</Link>
				</div>
		  	</div>
		
	 	 	<div className="content col-sm-10">
				<Route exact path="/" component={HomeContainer}/>
				<Route exact path="/about" component={AboutContainer}/>
				<Route exact path="/ask" component={AskContainer}/>
				<Route exact path="/questions" component={QuestionsContainer}/>
				<Route exact path="/questions/:id" component={MapContainer}/>
			</div>
		</div>
  	</Router>
);