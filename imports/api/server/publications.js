import { Meteor } from 'meteor/meteor';
import { Answers } from '../answers.js';
import { Questions } from '../questions.js';

Meteor.publish('answers.all', function answersAll() {
  return Answers.find({});
});

Meteor.publish('questions.all', function questionsAll() {
  return Questions.find({});
});

Meteor.publish('answers.request', function answersRequest(id) {
	/*
	countries.forEach(function(element, index, array){
	//result = [];
    //percent = 0;
    if(Answers.findOne({country: element, yes: true}) != null){
      percent = Answers.find({country: element, yes: true}).count() / Answers.find({country: element}).count();
      percent = percent * 100;
    }
    result.push([element, percent]);
    console.log(percent);
  });*/

  return Answers.find({_id: id});
});
