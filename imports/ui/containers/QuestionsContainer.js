import React, { Component } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import { Questions as QuestionsModel } from '../../api/questions.js';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom'
// App component - represents the whole app

class Questions extends Component {

  	renderTasks() {
    	return this.props.questions.map((task) => (
      		//<li key={task._id}>{task.text}</li> 
      		<Link to={"questions/" + task._id} key={task._id} className="question_item" params={{title: task.text}}>{task.text}</Link> 
    	));
  	}

    renderQuestions() {
    //return this.props.questions.map((question) => (
    //  <QuestionList key={question._id} questions={question} />
    //));

    //return getTasks().map((task) =>(
    	
    //	));

    }

	render() {
		return (
		  <div className="container">
		    <header>
		      <h1>Todo List</h1>
		    </header>

		    <ul>
		      {this.renderTasks()}
		    </ul>
		  </div>
		);
	}
}

Questions.propTypes = {
  questions: PropTypes.array.isRequired,
};

export default createContainer(() => {
	const questionsHandle = Meteor.subscribe('questions.all');
  return {
    questions: QuestionsModel.find({}).fetch(),
  };
}, Questions);
/*
class Questions extends Component{
  renderQuestions(){
  	let filteredQuestions = this.questions;
  	console.log(this.props);
  	return filteredQuestions.map((question) => (
      <QuestionList key={question._id} question={question} />
    ));
  }
  render(){
  	return (
  		<div className="questionlist">
  			<ul>{this.renderQuestions()}</ul>
  		</div>
  	);
  }
}

Questions.propTypes = {
  questions: PropTypes.array.isRequired,
};


export default QuestionsContainer = createContainer(() => {
  console.log(QuestionsModel.find({}).fetch());
  const questions = QuestionsModel.find({}).fetch();
  return {
    questions: questions,
  };
}, Questions);
*/