import React, { Component } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import { Questions } from '../../api/questions.js';
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types';
class AskClass extends React.Component{
  handleSubmit(event) {
    event.preventDefault();
 
    // Find the text field via the React ref
    const text = ReactDOM.findDOMNode(this.refs.textInput).value.trim();
 
    Questions.insert({
      text,
      //user._id,
      createdAt: new Date(), // current time
    });
 
    // Clear form
    ReactDOM.findDOMNode(this.refs.textInput).value = '';
  }
  render(){
    return(

      <div>
        <div id="menu">This is Ask form. Let's ask anything.</div>
        <form className="new-task" onSubmit={this.handleSubmit.bind(this)} >
          <input
            type="text"
            ref="textInput"
            placeholder="Type to add new tasks"
          />
        </form>
      </div>
    );
  }
}

export default AskContainer = createContainer(props => {
  return {
 
    user: Meteor.user(),
    //_id = this.props._id
    
  };
}, AskClass);