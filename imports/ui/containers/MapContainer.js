import React, { Component } from 'react';
import { createContainer } from 'meteor/react-meteor-data';
import { Questions } from '../../api/questions.js';
import PropTypes from 'prop-types';
import { Answers } from '../../api/answers.js';
import ReactDOM from 'react-dom';
import { Chart } from 'react-google-charts';

class MapClass extends React.Component{
  /*handleAlternate(event){
    event.preventDefault();
    const country = ReactDOM.findDOMNode(this.refs.textInput).value.trim();
 
    Answers.insert({
      country,
      yes: false,
      question_id:this.props.match._id,
      createdAt: new Date(), // current time
    });
    ReactDOM.findDOMNode(this.refs.textInput).value = '';
  }
  handleSubmit(event) {
    event.preventDefault();
    const country = ReactDOM.findDOMNode(this.refs.textInput).value.trim();
 
    Answers.insert({
      country,
      yes: true,
      question_id: this.props.match.params.id,
      createdAt: new Date(), // current time
    });
    ReactDOM.findDOMNode(this.refs.textInput).value = '';
  }*/

  handleAlternate(event){
    event.preventDefault();
    this.dataInsert(false);
    
  }
  handleSubmit(event) {
    event.preventDefault();
    this.dataInsert(true);
  }

  dataInsert(yesflag){
    const country = ReactDOM.findDOMNode(this.refs.textInput).value.trim();//for debug at the moment
    const id = this.props.match.params.id;
    if(Answers.findOne({country: country, question_id: id}) == null){//なければレコード作る
      Answers.insert({//なぜか新しいレコードができてしまう、インクリメントできない
        country,
        question_id: id,
        yes: 0,
        no: 0,
        createdAt: new Date(), // current time
      });
    }
    const doc = Answers.findOne({country: country, question_id: id});
    if(yesflag == true){//Yesボタンならyesをインクリメント、Noならnoをインクリメントする
      Answers.update({_id: doc._id}, 
        { $inc: {yes: 1}});
    }else{
      Answers.update({_id: doc._id}, 
        { $inc: {no: 1}});
    }

    ReactDOM.findDOMNode(this.refs.textInput).value = '';
  }

  constructor(props) {
    super(props);
    this.state = {
      options: {
        title: 'Age vs. Weight comparison',
      },
      data: [
        ['Country', 'Popularity'],
        ['South America', 12],
        ['Canada', 5.5],
        ['France', 14],
        ['Russia', 5],
        ['Australia', 3.5],
      ],
    };
    this.state.data.push(['China', 40]);
    console.log(this.props);
    console.log(this.state);
    //this.
  }

  render() {
    /*const {loading} = this.props;
    if(loading){
      return this.renderLoading();
    }*/
    return (

      <div>
        
        <h1>{this.props.title.text}</h1>

        <Chart
          chartType="GeoChart"
          data={this.state.data}
          options={this.state.options}
          graph_id="ScatterChart"
          width="900px"
          height="400px"
          legend_toggle
        />
        <form className="new-task" onSubmit={this.handleSubmit.bind(this)} >
          <input
            type="text"
            ref="textInput"
            placeholder="Type to add new tasks"
          />
          <button type="submit">Yes</button>
          <button onClick={this.handleAlternate.bind(this)}>No</button>
        </form>
      </div>
    );
  }

  renderLoading(){

  return(
    <div>
      Now Loading Database.
    </div>
    );
  }
}

//const callResult = 



export default MapContainer = createContainer(props => {
  const countries = ["Japan", "China", "Russia", "America"];
  //const answersHandle = Meteor.subscribe('answers.request', props.match.params.id);
  result = [];
  /*countries.forEach(function(element, index, array){
    console.log(Answers.findOne({country: element}));
    console.log(element);

    percent = 0;
    if(Answers.findOne({country: element, yes: true}) != null){
      percent = Answers.find({country: element, yes: true}).count() / Answers.find({country: element}).count();
      percent = percent * 100;
    }else{
      //percent = 0;
    }
    result.push([element, percent]);
    console.log(percent);
  });*/

  return {
    //loading: !answersHandle.ready(),
    title: Questions.findOne({_id: props.match.params.id}),
    //answer: Answers.find({_id: props.match.params.id}),
    result,
  };
}, MapClass);
